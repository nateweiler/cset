<!DOCTYPE html>

<head>
  <meta charset="utf-8">
  <title>Build a Simple, Static, Markdown-Powered Blog with Flask - James Harding</title>
  <meta name="description"
    content="A short tutorial explaining how to set up a static Markdown blog in Flask in just a few lines of Python.">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-8439586-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-8439586-1');
  </script>


  <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono|Open+Sans|Roboto:300,400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="/static/css/normalize-min.css">
  <link rel="stylesheet" href="/static/css/skeleton.css">
  <link rel="stylesheet" href="/static/css/fullpage.min.css">
  <link rel="stylesheet" href="/pygments.css">
  <link rel="stylesheet" href="/static/css/main.css">

  <script src="https://cdn.jsdelivr.net/npm/typeit@6.1.1/dist/typeit.min.js"></script>
  <script src="/static/js/headroom.min.js"></script>
  <script src="/static/js/min/fullpage-min.js"></script>
  <script src="/static/js/min/scripts-min.js"></script>
</head>

<body>
  <header id="header">
    <div class="container">
      <div class="five columns">
        <h1 class="title"><span class="title_lt">></span> James Harding</h1>
      </div>
      <div class="seven columns">
        <nav class="top_menu">
            <ul id="fp-menu">
              <li data-menuanchor="intro"><a href="/#intro">Home</a></li>
              <li data-menuanchor="pilot"><a href="/#pilot">Pilot</a></li>
              <li data-menuanchor="engineer"><a href="/#engineer">Engineer</a></li>
              <li data-menuanchor="posts"><a href="/#posts">Posts</a></li>
              <li data-menuanchor="connect"><a href="/#connect">Connect</a></li>
            </ul>
        </nav>
      </div>
    </div>
    
  </header>
  
<div class="contentblock post first_block">
  <div class="container">
    <h2>Build a Simple, Static, Markdown-Powered Blog with Flask</h2>
    <div class="row">
      <div class="post twelve columns">
        <p>For years, I used and adored Wordpress as my go-to blogging platform. It was quick to install, easy to use, and had a ton of <s>bloat</s> functionality that I simply never used. Flask has been my framework of choice when starting a new web project for a while now, and I wanted a dead-simple solution for managing my mostly static site and handling my <em>occasional</em> writing.  I had the following requirements:</p>
<ul>
<li>Must build into static site</li>
<li>Posts composed in Markdown &lt;3</li>
<li>Ability to include code snippets</li>
</ul>
<p>Before we begin, I recommend having a quick read of the <a href="http://flask.pocoo.org/docs/quickstart/">Flask Documentation</a> to familiarise yourself with the basics of Flask.</p>
<h3>Ingredients</h3>
<p>Let's start with our Python dependencies. We are using Flask 0.10.x and Python 2.7</p>
<div class="codehilite"><pre><span></span><span class="n">Click</span><span class="o">==</span><span class="mi">7</span><span class="p">.</span><span class="mi">0</span>
<span class="n">Flask</span><span class="o">==</span><span class="mi">1</span><span class="p">.</span><span class="mi">1</span><span class="p">.</span><span class="mi">1</span>
<span class="n">Flask</span><span class="o">-</span><span class="n">FlatPages</span><span class="o">==</span><span class="mi">0</span><span class="p">.</span><span class="mi">7</span><span class="p">.</span><span class="mi">1</span>
<span class="n">Frozen</span><span class="o">-</span><span class="n">Flask</span><span class="o">==</span><span class="mi">0</span><span class="p">.</span><span class="mi">15</span>
<span class="n">itsdangerous</span><span class="o">==</span><span class="mi">1</span><span class="p">.</span><span class="mi">1</span><span class="p">.</span><span class="mi">0</span>
<span class="n">Jinja2</span><span class="o">==</span><span class="mi">2</span><span class="p">.</span><span class="mi">10</span><span class="p">.</span><span class="mi">3</span>
<span class="n">Markdown</span><span class="o">==</span><span class="mi">3</span><span class="p">.</span><span class="mi">1</span><span class="p">.</span><span class="mi">1</span>
<span class="n">MarkupSafe</span><span class="o">==</span><span class="mi">1</span><span class="p">.</span><span class="mi">1</span><span class="p">.</span><span class="mi">1</span>
<span class="n">Pygments</span><span class="o">==</span><span class="mi">2</span><span class="p">.</span><span class="mi">4</span><span class="p">.</span><span class="mi">2</span>
<span class="n">PyYAML</span><span class="o">==</span><span class="mi">5</span><span class="p">.</span><span class="mi">1</span><span class="p">.</span><span class="mi">2</span>
<span class="n">Werkzeug</span><span class="o">==</span><span class="mi">0</span><span class="p">.</span><span class="mi">16</span><span class="p">.</span><span class="mi">0</span>
</pre></div>


<p>I will assume that you know a little about <a href="http://iamzed.com/2009/05/07/a-primer-on-virtualenv/">venv</a>. If not, you seriously should be using it for all of your Python projects. Drop the above list into a file named requirements.txt and execute <code>pip install -r requirements.txt</code> from your shell. You are now ready to start building your blog.</p>
<h3>Folder Structure</h3>
<p>We need to create folders to store our static markdown pages, in addition to our standard templates and static files folders.</p>
<div class="codehilite"><pre><span></span>├── blog.py
├── content
│   └── posts
│       └── firstpost.md
├── static
└── templates

4 directories, 2 files
</pre></div>


<p>Your stylesheets and javascript files will go in the static directory, and all of your posts will go in the… I think that you can probably guess which folder :)</p>
<h3>The Routes File</h3>
<p>Flask makes it dead simple to declare a pretty URL scheme for your blog with the route wrapper functions. To begin, we need to make sure that we import all of the required packages. Create a new file named <code>blog.py</code> and open it up. It should look something like this...</p>
<div class="codehilite"><pre><span></span><span class="kn">import</span> <span class="nn">sys</span>
<span class="kn">from</span> <span class="nn">flask</span> <span class="kn">import</span> <span class="n">Flask</span><span class="p">,</span> <span class="n">render_template</span>
<span class="kn">from</span> <span class="nn">flask_flatpages</span> <span class="kn">import</span> <span class="n">FlatPages</span><span class="p">,</span> <span class="n">pygments_style_defs</span>
<span class="kn">from</span> <span class="nn">flask_frozen</span> <span class="kn">import</span> <span class="n">Freezer</span>

<span class="n">DEBUG</span> <span class="o">=</span> <span class="bp">True</span>
<span class="n">FLATPAGES_AUTO_RELOAD</span> <span class="o">=</span> <span class="n">DEBUG</span>
<span class="n">FLATPAGES_EXTENSION</span> <span class="o">=</span> <span class="s1">&#39;.md&#39;</span>
<span class="n">FLATPAGES_ROOT</span> <span class="o">=</span> <span class="s1">&#39;content&#39;</span>
<span class="n">POST_DIR</span> <span class="o">=</span> <span class="s1">&#39;posts&#39;</span>

<span class="n">app</span> <span class="o">=</span> <span class="n">Flask</span><span class="p">(</span><span class="vm">__name__</span><span class="p">)</span>
<span class="n">flatpages</span> <span class="o">=</span> <span class="n">FlatPages</span><span class="p">(</span><span class="n">app</span><span class="p">)</span>
<span class="n">freezer</span> <span class="o">=</span> <span class="n">Freezer</span><span class="p">(</span><span class="n">app</span><span class="p">)</span>
<span class="n">app</span><span class="o">.</span><span class="n">config</span><span class="o">.</span><span class="n">from_object</span><span class="p">(</span><span class="vm">__name__</span><span class="p">)</span>

<span class="k">if</span> <span class="vm">__name__</span> <span class="o">==</span> <span class="s2">&quot;__main__&quot;</span><span class="p">:</span>
    <span class="k">if</span> <span class="nb">len</span><span class="p">(</span><span class="n">sys</span><span class="o">.</span><span class="n">argv</span><span class="p">)</span> <span class="o">&gt;</span> <span class="mi">1</span> <span class="ow">and</span> <span class="n">sys</span><span class="o">.</span><span class="n">argv</span><span class="p">[</span><span class="mi">1</span><span class="p">]</span> <span class="o">==</span> <span class="s2">&quot;build&quot;</span><span class="p">:</span>
        <span class="n">freezer</span><span class="o">.</span><span class="n">freeze</span><span class="p">()</span>
    <span class="k">else</span><span class="p">:</span>
        <span class="n">app</span><span class="o">.</span><span class="n">run</span><span class="p">(</span><span class="n">host</span><span class="o">=</span><span class="s1">&#39;0.0.0.0&#39;</span><span class="p">,</span> <span class="n">debug</span><span class="o">=</span><span class="bp">True</span><span class="p">)</span>
</pre></div>


<p>Great, we have the beginnings of our blog. Time to add some routes that will serve our blog posts. You can <a href="http://flask.pocoo.org/docs/quickstart/#routing">add your own routes</a> for custom pages here too. We will define the <em>posts</em> route to find and list all of our posts, and <em>post</em> will create a page for each one. Let's add the following immediately before the <code>if __name__ == "__main__"</code> line.</p>
<div class="codehilite"><pre><span></span><span class="nd">@app.route</span><span class="p">(</span><span class="s2">&quot;/posts/&quot;</span><span class="p">)</span>
<span class="k">def</span> <span class="nf">posts</span><span class="p">():</span>
    <span class="n">posts</span> <span class="o">=</span> <span class="p">[</span><span class="n">p</span> <span class="k">for</span> <span class="n">p</span> <span class="ow">in</span> <span class="n">flatpages</span> <span class="k">if</span> <span class="n">p</span><span class="o">.</span><span class="n">path</span><span class="o">.</span><span class="n">startswith</span><span class="p">(</span><span class="n">POST_DIR</span><span class="p">)]</span>
    <span class="n">posts</span><span class="o">.</span><span class="n">sort</span><span class="p">(</span><span class="n">key</span><span class="o">=</span><span class="k">lambda</span> <span class="n">item</span><span class="p">:</span><span class="n">item</span><span class="p">[</span><span class="s1">&#39;date&#39;</span><span class="p">],</span> <span class="n">reverse</span><span class="o">=</span><span class="bp">False</span><span class="p">)</span>
    <span class="k">return</span> <span class="n">render_template</span><span class="p">(</span><span class="s1">&#39;posts.html&#39;</span><span class="p">,</span> <span class="n">posts</span><span class="o">=</span><span class="n">posts</span><span class="p">)</span>

<span class="nd">@app.route</span><span class="p">(</span><span class="s1">&#39;/posts/&lt;name&gt;/&#39;</span><span class="p">)</span>
<span class="k">def</span> <span class="nf">post</span><span class="p">(</span><span class="n">name</span><span class="p">):</span>
    <span class="n">path</span> <span class="o">=</span> <span class="s1">&#39;{}/{}&#39;</span><span class="o">.</span><span class="n">format</span><span class="p">(</span><span class="n">POST_DIR</span><span class="p">,</span> <span class="n">name</span><span class="p">)</span>
    <span class="n">post</span> <span class="o">=</span> <span class="n">flatpages</span><span class="o">.</span><span class="n">get_or_404</span><span class="p">(</span><span class="n">path</span><span class="p">)</span>
    <span class="k">return</span> <span class="n">render_template</span><span class="p">(</span><span class="s1">&#39;post.html&#39;</span><span class="p">,</span> <span class="n">post</span><span class="o">=</span><span class="n">post</span><span class="p">)</span>
</pre></div>


<p>The first route will iterate over the list of posts, and pass this to the template which we will look at shortly. I chose to sort by date. 10 lines of code, and this covers all of the <em>blog</em> logic that we have to deal with.</p>
<h3>Templates</h3>
<p>Start by making a new template for the list of posts. I will give you a bare minimal snippet, beyond that it is up to you! We will name this file <strong>posts.html</strong> and place it in the templates directory:</p>
<div class="codehilite"><pre><span></span><span class="cp">{%</span> <span class="k">for</span> <span class="nv">post</span> <span class="k">in</span> <span class="nv">posts</span> <span class="cp">%}</span><span class="x"></span>
<span class="x">    &lt;a href=&quot;</span><span class="cp">{{</span> <span class="nv">url_for</span><span class="o">(</span><span class="s1">&#39;post&#39;</span><span class="o">,</span> <span class="nv">name</span><span class="o">=</span><span class="nv">post.path.replace</span><span class="o">(</span><span class="s1">&#39;posts/&#39;</span><span class="o">,</span> <span class="s1">&#39;&#39;</span><span class="o">))</span> <span class="cp">}}</span><span class="x">&quot;&gt;</span>
<span class="x">        </span><span class="cp">{{</span> <span class="nv">post.title</span> <span class="cp">}}</span><span class="x"></span>
<span class="x">    &lt;/a&gt;</span>
<span class="x">    &lt;small&gt;</span><span class="cp">{{</span> <span class="nv">post.date</span> <span class="cp">}}</span><span class="x">&lt;/small&gt;</span>
<span class="cp">{%</span> <span class="k">endfor</span> <span class="cp">%}</span><span class="x"></span>
</pre></div>


<p>And for <strong>post.html</strong>, drop the following into your template wherever you want them displayed!</p>
<div class="codehilite"><pre><span></span><span class="cp">{{</span> <span class="nv">post.html</span><span class="o">|</span><span class="nf">safe</span> <span class="cp">}}</span><span class="x"></span>
<span class="cp">{{</span> <span class="nv">post.title</span> <span class="cp">}}</span><span class="x"></span>
<span class="cp">{{</span> <span class="nv">post.date</span> <span class="cp">}}</span><span class="x"></span>
</pre></div>


<p>Now you're cooking with a Flask!</p>
<h3>How About The Posts</h3>
<p>Oh yeah, this is the easy part. If you are on Mac, I would recommend <a href="http://mouapp.com/">Mou</a> as your editor, and for those on Windows <a href="http://markdownpad.com/">MarkdownPad</a> comes highly commended. You will store your markdown files in the <code>content/posts/</code> directory as <code>some_file_name.md</code>, and include a small amount of metadata in each file.</p>
<div class="codehilite"><pre><span></span>title: Should it be YYYY-MM-DD or YYYY-DD-MM? My Thoughts
date: 2013-08-27

American-style dates, I know, I know. Here you can write markdown code, including *italics* and **bold**

You can have:
* Lists
* More Lists

&gt; And block quotes

    And event highlighted code if you indent :)
</pre></div>


<h3>Lets Build This Thing</h3>
<p>If you want to run this server to test on your local machine, you can simply run the script using <code>python blog.py</code>.</p>
<p>When you are happy and want to generate the static site, use <code>python blog.py build</code> and a new <strong>build</strong> directory will be created with your new static blog.</p>
<p>Deploy away :)</p>
      </div>
    </div>
    <div class="row">
      <div class="comments twelve columns">
        <div id="disqus_thread"></div>
        <script type="text/javascript">
          /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
          var disqus_shortname = 'jamesharding'; // required: replace example with your forum shortname

          /* * * DON'T EDIT BELOW THIS LINE * * */
          (function () {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
          })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by
            Disqus.</a></noscript>
        <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
      </div>
    </div>
  </div>
</div>


</body>

</html>