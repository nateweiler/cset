Python 3.7.5 (default, Nov  1 2019, 02:16:23) 
[Clang 11.0.0 (clang-1100.0.33.8)] on darwin
Type "help", "copyright", "credits" or "license()" for more information.
WARNING: The version of Tcl/Tk (8.5.9) in use may be unstable. Visit
http://www.python.org/download/mac/tcltk/ for current information.
>>> Exercise 06
print("Mary had a little lamb,")
print("Its fleece was white as {},".format('snow'))
print("And everywhere that Mary went,")
print("." * 10) # what???
a = 'C'
b = 'h'
c = 'e'
d = 'e'
e = 's'
f = 'e'
g = 'b'
h = 'u'
i = 'r'
j = 'g'
k = 'e'
l = 'r'
print(a + b + c + d + e + f, end=' ')
print(g + h + i + j + k + l)
